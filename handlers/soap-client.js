const soap = require('soap');

function createClient(url) {

    return new Promise(function(resolve, reject) {
        soap.createClient(url, function(err, client) {
            if (err) {
                console.log(err);
            } else {
                resolve(client);
            }    

        })    
    })
}

exports.createClient = createClient;