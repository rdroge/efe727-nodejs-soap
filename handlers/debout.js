const config = require('../config.json'); 
const client = require('./soap-client');
const login = require('../login');
const url = config.baseUrl + 'debout';

function getDeboutsByDebtorId(context, complete, modules) {
    var loginPromise = login.getSecKey();
        loginPromise.then(function(result) {
            var secKey = result;
            var args = {    pi_csecurity_data: secKey,   
                            pi_dscontext3: {
                                itmpDebout: {
                                    attributes: {
                                    adm_nr: config.admnr,
                                    debtor: context.entityId,
                                    entry_nr_type: "1"        
                                    }
                                }
                            }
                        }

            console.log(args);                
            var soapclient = client.createClient(url);         

            soapclient.then(function (result) {
                var debout = result; 
                debout.search(args, function(err, result) {
                    if (err) {
                        //console.log('error', err);  
                    } else {
                        //console.log('result', result);
                        return complete().setBody(result.po_dscontext2.otmpDebout).ok().done();
                    }
                })
            }) 
        }), function(err) {
                console.log(err);
        }
}

function getDeboutsByQuery(context, complete, modules) {
    console.log(context);
    var loginPromise = login.getSecKey();
    loginPromise.then(function(result) {
        
        var blaat = context.query.query.replace(/[{}]/g, "");;
        var secKey = result;
        var args = '{ "pi_csecurity_data":' + JSON.stringify(secKey) + ', "pi_dscontext2": { "itmpDebout": { "attributes": { "adm_nr":' + config.admnr + '},' + blaat + '}}}';
        
        args = JSON.parse(args);
         

        var soapclient = client.createClient(url);         

        soapclient.then(function (result) {
            var debout = result;
            debout.search(args, function(err, result) {
                console.log('error', err, 'result', result);
                if (err) {
                    return complete().setBody(err).runtimeError().done();
                  } else if (err == null && result === undefined) {
                    return complete().notFound().done();
                  }
                  return complete().setBody(result.po_dscontext3.otmpDebout).ok().next();
            })
        })
    }), function(err) {
            console.log(err);
    }
}

exports.getDeboutsByDebtorId = getDeboutsByDebtorId;
exports.getDeboutsByQuery = getDeboutsByQuery;


