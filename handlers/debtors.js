const config = require('../config.json'); 
const client = require('./soap-client');
const login = require('../login');
const url = config.baseUrl + 'debtor';

function getDebtorById(context, complete, modules) {
    var loginPromise = login.getSecKey();
        loginPromise.then(function(result) {
            
            var secKey = result;
            var args = {    pi_csecurity_data: secKey,   
                            pi_dscontext5: {
                                itmpDebtor: {
                                    attributes: {
                                    adm_nr: config.admnr,
                                    debtor: context.entityId        
                                    }
                                }
                            }
                        }

            console.log(args);                
            var soapclient = client.createClient(url);         

            soapclient.then(function (result) {
                var debtor = result;
                debtor.find(args, function(err, result) {
                    if (err) {
                        return complete().setBody(err).runtimeError().done();
                    } else if (err == null && result === undefined) {
                        return complete().notFound().done();  
                    } else {
                        //console.log('result', result);
                        return complete().setBody(result.po_dscontext.otmpDebtor[0]).ok().done();
                    }
                })
            })
        }), function(err) {
                console.log(err);
        }
}

function getDebtorsByQuery(context, complete, modules) {
    console.log(context);
    var loginPromise = login.getSecKey();
    loginPromise.then(function(result) {
        
        var querystring = context.query.query.replace(/[{}]/g, "");
        var querystring = querystring.replace(/'/g,'"');
        var position = querystring.length-1;
        querystring = [querystring.slice(0, position), "*", querystring.slice(position)].join('');
        console.log(querystring);
        var secKey = result;
        var args = '{ "pi_csecurity_data":' + JSON.stringify(secKey) + ', "pi_dscontext6": { "itmpDebtor": { "attributes": { "adm_nr":' + config.admnr + '},' + querystring + '}}}';
        
        args = JSON.parse(args);
         

        var soapclient = client.createClient(url);         

        soapclient.then(function (result) {
            var debtor = result;
            debtor.search(args, function(err, result) {
               
                if (err) {
                    return complete().setBody(err).runtimeError().done();
                  } else if (err == null && result === undefined) {
                    return complete().notFound().done();
                } else {
                    if (result.po_dscontext3.otmpDebtor.length > 1) {
                        return complete().setBody(result.po_dscontext3).ok().next();
                    } else {
                        return complete().setBody(result.po_dscontext3.otmpDebtor[0]).ok().next();    
                    }
                }

            })
        })
    }), function(err) {
            console.log(err);
    }
}

function updateDebtorById(context, complete, modules) {
        var loginPromise = login.getSecKey();
        loginPromise.then(function(result) {
            var adresvalue;
            var telephonevalue;
            console.log(context);
            if (context.body.address_1 == '') {
                adresvalue = null;
                telephonevalue = context.body.telephone;     
            } else {
                adresvalue = context.body.address_1;
                telephonevalue = null;
            }

            var secKey = result;
            var args = {    pi_csecurity_data: secKey,   
                            pi_dscontext4: {
                                itmpDebtor: {
                                    attributes: {
                                    adm_nr: config.admnr,
                                    debtor: context.entityId        
                                    },
                                    address_1: adresvalue,
                                    telephone: telephonevalue
                                },
                                ttvariables: {
                                    field_list: 'telephone,address_1'
                                }
                            }
                        }

            console.log(args);                
            var soapclient = client.createClient(url);         
                        
            soapclient.then(function (result) {
                var debtor = result;
                debtor.update(args, function(err, result) {
                    if (err) {
                        return complete().setBody(err).runtimeError().done();
                    } else if (err == null && result === undefined) {
                        return complete().notFound().done();  
                    } else {
                        //console.log('result', result);
                        return complete().setBody(result.po_dscontext.otmpDebtor[0]).ok().done();
                    }
                })
            })
        }), function(err) {
                console.log(err);
        }
    
}

exports.getDebtorById = getDebtorById;
exports.getDebtorsByQuery = getDebtorsByQuery;
exports.updateDebtorById = updateDebtorById;

