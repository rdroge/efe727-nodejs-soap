var soap = require('soap');
const config = require('./config.json'); 
var url = config.baseUrl + 'login';
var args = { 'pi_cUserID': 'super', 'pi_cPassword': ''};


function getSecKey() {

    return new Promise(function(resolve, reject) {

        soap.createClient(url, function(err, client) {
            
            if (err) {
                console.log("error @ login", err);
            } else {
            client.login(args, function(err, result) {
                
                resolve(result.po_cSecurity_data);
            
            });
        }
        });
    })
}

exports.getSecKey = getSecKey;
