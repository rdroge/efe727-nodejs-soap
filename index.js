const sdk = require('kinvey-flex-sdk');
const debtors = require('./handlers/debtors');
const debouts = require('./handlers/debout');

sdk.service((err, flex) => {
    const data = flex.data;
    const debtor = data.serviceObject('debtor');
    const debout = data.serviceObject('debout'); 


    
        // set the serviceObject
        
        // wire up the event that we want to process
    debtor.onGetById(debtors.getDebtorById);
    debtor.onGetByQuery(debtors.getDebtorsByQuery);
    debout.onGetByQuery(debouts.getDeboutsByQuery);
    debout.onGetById(debouts.getDeboutsByDebtorId);
    debtor.onUpdate(debtors.updateDebtorById);
})

